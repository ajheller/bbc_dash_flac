#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Tue Jul 18 12:15:16 2017

@author: heller
"""
from __future__ import print_function, division
import requests
from xml.dom import minidom

import time
import calendar
import os
import sys


"""
url fetched at 7/19/2017 19:30 ET
https://vs-dash-ww-rd-live.bbcfmt.hs.llnwd.net/al/lossless/A1/390759417.m4s

segment is 

int(calendar.timegm(time.gmtime())/3.84)
"""


manifest_str = """<?xml version="1.0" encoding="UTF-8"?>
<MPD type="dynamic" xmlns="urn:mpeg:dash:schema:mpd:2011" xmlns:dvb="urn:dvb:dash-extensions:2014-1" profiles="urn:dvb:dash:profile:dvb-dash:2014,urn:dvb:dash:profile:dvb-dash:isoff-ext-live:2014" minBufferTime="PT2.034S" maxSegmentDuration="PT3.84S" timeShiftBufferDepth="PT10M" minimumUpdatePeriod="PT1H" availabilityStartTime="1970-01-01T00:01:00Z"  publishTime="2017-07-12T10:14:21Z">
	<!-- MPEG DASH ISO BMFF test stream -->
	<!-- Lossless audio using FLAC -->
	<!-- BBC Research & Development -->
	<!-- For more information see http://rdmedia.bbc.co.uk -->
	<!-- Email dash@rd.bbc.co.uk -->
	<!-- (c) British Broadcasting Corporation 2017.  All rights reserved.-->
	<ProgramInformation>
		<Title>Radio 3 lossless</Title>
		<Source>BBC Research and Development</Source>
		<Copyright>British Broadcasting Corporation 2017</Copyright>
	</ProgramInformation>
    <UTCTiming schemeIdUri="urn:mpeg:dash:utc:http-xsdate:2014" value="http://time.akamai.com/?iso"/>
	<Period start="PT0S">
		<AdaptationSet startWithSAP="2" segmentAlignment="true" id="1" codecs="flac" audioSamplingRate="48000" lang="eng" mimeType="audio/mp4">
			<AudioChannelConfiguration schemeIdUri="urn:mpeg:dash:23003:3:audio_channel_configuration:2011" value="2"/>
			<Role schemeIdUri="urn:mpeg:dash:role:2011" value="main"/>
			<SegmentTemplate timescale="48000" duration="184320" media="$RepresentationID$/$Number%06d$.m4s" initialization="$RepresentationID$/IS.mp4" startNumber="1" presentationTimeOffset="0"/>
			<Representation id="A1" bandwidth="1536000"/>
		</AdaptationSet>
	</Period>
	<Metrics metrics="DVBErrors">
		<Reporting schemeIdUri="urn:dvb:dash:reporting:2014" value="1" dvb:reportingUrl="http://rdmedia.bbc.co.uk/dash/errorreporting/reporterror.php" dvb:probability="50"/>
	</Metrics>
</MPD>
"""

base_url = "https://vs-dash-ww-rd-live.bbcfmt.hs.llnwd.net/al/lossless/"

def get_manifest():
    r = requests.get(base_url + "client_manifest.mpd")
    if r.ok:
        manifest_xml = minidom.parseString(r.content)
        return manifest_xml.getElementsByTagName("MPD")[0] # there is only one
    else:
        print(r.status_code, r.reason)
        return None

def get_tag_attributes(tagName):
    element = mpd.getElementsByTagName(tagName)[0]
    attributes = element.attributes
    print()
    print(tagName)
    if True:
        for k in attributes.keys():
            print(k, attributes[k].value)
    return attributes

def get_tag_data(mpd, tag_name):
    return mpd.getElementsByTagName(tag_name)[0].childNodes[0].data

def utc():
    return calendar.timegm(time.gmtime())



    

#print get_tag_attributes("MPD")
#print get_tag_attributes("AdaptationSet")
#print get_tag_attributes("AudioChannelConfiguration")
#print get_tag_attributes("SegmentTemplate")

def stream_cap(duration=None, output_file_base=None):
    if duration is None:
        duration = 2 * 3600  # seconds
        
    if output_file_base is None:
        output_file_base = "bbc_r3_lossless-" + time.strftime('%Y%m%d_%H%M%S', time.gmtime())
        
    end_utc = utc() + duration
    
    mpd = get_manifest()
    print("\nSource --> " + get_tag_data(mpd, "Source"))
    print("Title  --> " + get_tag_data(mpd, "Title"))
        
    while utc() <= end_utc:
        http_error_count = 0
        segment_no = int((utc()-60)/3.84) # live segments take about a minute to appear on server
        
        output_file_mp4 = output_file_base + ".mp4"
        print("writing to: " + output_file_mp4)
        print("until: " + time.ctime(end_utc))
        
        with file(output_file_base + ".mp4", 'w') as f:
            initial_segment_url = base_url + "A1/IS.mp4"
            r = requests.get(initial_segment_url)
            if r.ok:
                print(initial_segment_url, len(r.content))
                f.write(r.content)
                f.flush()
            else:
                print(initial_segment_url, r.status_code, r.reason)
                
            while utc() <= end_utc and http_error_count < 10:
                segment_url = base_url + "A1/" + str(segment_no) + ".m4s"
                r = requests.get(segment_url)
                if r.ok:
                    print(segment_url, len(r.content))
                    f.write(r.content)
                    http_error_count = 0
                    segment_no += 1
                else:
                    http_error_count += 1
                    print(segment_url, r.status_code, r.reason, http_error_count)
                    time.sleep(5)
        
        output_file_flac = output_file_base + ".flac"
        print("converting " + output_file_mp4 + " to " + output_file_flac)
        try:
            os.system("ffmpeg -i " + output_file_mp4 + 
                      " -acodec copy " + 
                      output_file_flac)
        except Exception as ex:
            print(ex)
        else:
            print("Success!")


if __name__ == "__main__":
    args = len(sys.argv)
    
    if args == 1:
        stream_cap()
    elif args == 2:
        stream_cap(duration=int(sys.argv[1]))
    elif args == 3:
        stream_cap(duration=int(sys.argv[1]), output_file_base=sys.argv[2])
    else:
        print("dash_flac.py duration_secs out_file_base")
             




